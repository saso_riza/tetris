# Tetris game

Tetris game made with vanilla js

 [View demo here](https://tetrissss.netlify.app/)


[![Netlify Status](https://api.netlify.com/api/v1/badges/217698af-7c51-4cb1-9e85-d0ed4195d66b/deploy-status)](https://app.netlify.com/sites/tetrissss/deploys)


## Tutorial

[Code Tetris](https://www.youtube.com/watch?v=rAUn1Lom6dw)


## What i learned
- Thinking from a different perspective while coding for games

