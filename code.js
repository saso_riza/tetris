// https://www.youtube.com/watch?v=rAUn1Lom6dw
document.addEventListener("DOMContentLoaded", () => {
	// 200 tiles
	const gridWidth = 220;
	const gridHeight = 441;
	const minigridWidth = 88;
	const tileWidth = 20;
	const indent = 10; //in index
	const grid = document.getElementById("grid");
	const minigrid = document.getElementById("minigrid");
	const scoreDisplay = document.querySelector("#score");
	const linesDisplay = document.querySelector("#lines");
	const levelDisplay = document.querySelector("#level");
	const buttonPause = document.querySelector("#buttonPause");
	const buttonRestart = document.querySelector("#buttonrestart");
	let currentPosition = 4;
	let currentRotation = 0;
	let minigridIndex = 0;
	let timerId;
	let score = 0;
	let lines = 0;
	let level = 0;
	let speed = 1000;
	const colors = ["orange", "red", "yellow", "green", "blue"];
	//const colors = ["#f9844a", "#df2882", "#fec11d", "#66c73a", "#0197af"];

	// Creating the board
	// -- container
	grid.setAttribute("style", `width:${gridWidth}px;height:${gridHeight}px;`);
	// -- separate fields
	for (let i = 0; i < 210; i++) {
		const tile = document.createElement("DIV");
		tile.setAttribute("style", `width:${tileWidth}px;height:${tileWidth}px;`);
		// add bottom class
		if (i > 199) tile.classList.add("taken");
		grid.appendChild(tile);
	}
	// -- the "Next up" minigrid
	minigrid.setAttribute(
		"style",
		`width:${minigridWidth}px;height:${minigridWidth}px;`,
	);
	for (let i = 0; i < 16; i++) {
		const tile = document.createElement("DIV");
		tile.setAttribute("style", `width:${tileWidth}px;height:${tileWidth}px;`);
		tile.classList.add("tet");
		minigrid.appendChild(tile);
	}

	let squares = Array.from(document.querySelectorAll("#grid div"));
	let minigridSquares = Array.from(document.querySelectorAll("#minigrid div"));

	// the tetrominos' index x and y
	// ■
	// ■
	// ■ ■
	const tetrominoL = [
		[01, 11, 21, 02],
		[10, 11, 12, 22],
		[01, 11, 21, 20],
		[10, 20, 21, 22],
	];
	// ■ ■
	//   ■ ■
	const tetrominoZ = [
		[00, 10, 11, 21],
		[11, 12, 20, 21],
		[00, 10, 11, 21],
		[11, 12, 20, 21],
	];
	// ■ ■ ■
	//   ■
	const tetrominoT = [
		[01, 10, 11, 12],
		[01, 11, 12, 21],
		[10, 11, 12, 21],
		[01, 10, 11, 21],
	];
	// ■ ■
	// ■ ■
	const tetrominoO = [
		[00, 01, 10, 11],
		[00, 01, 10, 11],
		[00, 01, 10, 11],
		[00, 01, 10, 11],
	];
	// ■ ■ ■ ■
	const tetrominoI = [
		[01, 11, 21, 31],
		[10, 11, 12, 13],
		[01, 11, 21, 31],
		[10, 11, 12, 13],
	];

	const tetrominos = [
		tetrominoL,
		tetrominoZ,
		tetrominoT,
		tetrominoO,
		tetrominoI,
	];

	const upNextTetrominos = [
		[01, 05, 09, 02],
		[10, 04, 05, 09],
		[05, 08, 09, 10],
		[05, 06, 09, 10],
		[01, 05, 09, 13],
	];

	// random tetromino
	let nextRandom = Math.floor(Math.random() * tetrominos.length);
	let random = nextRandom;
	let currentTetromino = tetrominos[random][currentRotation];

	const draw = () =>
		currentTetromino.forEach((i) => {
			squares[currentPosition + i].classList.add("tetromino");
			squares[currentPosition + i].classList.add(colors[random]);
		});

	const undraw = () =>
		currentTetromino.forEach((i) => {
			squares[currentPosition + i].classList.remove("tetromino");
			squares[currentPosition + i].classList = "";
		});

	// vvv vvv vvv vvv vvv vvv vvv vvv vvv vvv vvv vvv vvv vvv
	// move the tetromino down
	//  vvv vvv vvv vvv vvv vvv vvv vvv vvv vvv vvv vvv vvv vvv
	const moveDown = () => {
		undraw();
		currentPosition += indent;
		draw();
		freeze();
	};

	// freeze when touching taken
	const freeze = () => {
		if (
			currentTetromino.some((i) =>
				squares[currentPosition + i + indent].classList.contains("taken"),
			)
		) {
			var audio = new Audio("assets/3.mp3");
			audio.play();
			currentTetromino.forEach((i) =>
				squares[currentPosition + i].classList.add("taken"),
			);
			// reset and start a new tetromino
			random = nextRandom;
			nextRandom = Math.floor(Math.random() * tetrominos.length);
			currentTetromino = tetrominos[random][currentRotation];
			currentPosition = 4;
			draw();
			miniDisplay();
			addScore();
			gameOver();
		}
	};

	// <<<-- <<<-- <<<-- <<<-- <<<-- <<<-- <<<-- <<<-- <<<--
	// move the tetromino left, unless there is a border or a "taken" tile
	// <<<-- <<<-- <<<-- <<<-- <<<-- <<<-- <<<-- <<<-- <<<--
	const moveLeft = () => {
		//index 9, 19, 29, 39 etc
		const isAtLeftBorder = currentTetromino.some(
			(i) => (currentPosition + i) % 10 === 0,
		);
		undraw();
		// move index position
		if (!isAtLeftBorder) currentPosition -= 1;
		// undo move if there is another tile in the way
		if (
			currentTetromino.some((i) =>
				squares[currentPosition + i].classList.contains("taken"),
			)
		)
			currentPosition += 1;
		draw();
	};
	// -->>> -->>> -->>> -->>> -->>> -->>> -->>> -->>> -->>>
	// move the tetromino right, unless there is a border  or a "taken" tile
	// -->>> -->>> -->>> -->>> -->>> -->>> -->>> -->>> -->>>
	const moveRight = () => {
		//index 0, 10, 20, 30 etc
		const isAtRightBorder = currentTetromino.some(
			(i) => (currentPosition + i) % 10 === 9,
		);
		undraw();
		// move index position
		if (!isAtRightBorder) currentPosition += 1;
		// undo move if there is another tile in the way
		if (
			currentTetromino.some((i) =>
				squares[currentPosition + i].classList.contains("taken"),
			)
		)
			currentPosition -= 1;
		draw();
	};

	// rotation
	const rotate = () => {
		const isAtRightBorder = currentTetromino.some(
			(i) => (currentPosition + i) % 10 === 9,
		);
		const isAtLeftBorder = currentTetromino.some(
			(i) => (currentPosition + i) % 10 === 0,
		);
		//if (isAtRightBorder || isAtLeftBorder) return;

		undraw();

		currentRotation++;
		// change to 0 if end has been reached
		if (currentRotation === currentTetromino.length) currentRotation = 0;

		currentTetromino = tetrominos[random][currentRotation];
		draw();
	};

	// KEYCODES
	// https://keycode.info/
	const control = (e) => {
		if (e.keyCode === 37) moveLeft();
		if (e.keyCode === 38) rotate();
		if (e.keyCode === 39) moveRight();
		if (e.keyCode === 40) moveDown();
	};
	//document.addEventListener("keyup", control);
	document.addEventListener("keydown", control);

	const miniDisplay = () => {
		minigridSquares.forEach((tile) => {
			tile.classList.remove("tetromino");
			tile.classList.remove(colors[nextRandom]);
			tile.classList = "";
			tile.style.backgroundColor = "";
		});
		upNextTetrominos[nextRandom].forEach((i) => {
			minigridSquares[minigridIndex + i].classList.add("tetromino");
			minigridSquares[minigridIndex + i].classList.add(colors[nextRandom]);
		});
	};

	//Button
	buttonPause.addEventListener("click", () => {
		if (timerId) {
			clearInterval(timerId);
			buttonPause.classList.add("active");
			timerId = null;
		} else {
			draw();
			timerId = setInterval(moveDown, speed);
			buttonPause.classList.remove("active");
			//nextRandom = Math.floor(Math.random() * tetrominos.length);
			miniDisplay();
		}
	});
	//Button RESTART
	buttonRestart.addEventListener("click", () => {
		window.location.reload();
	});

	const addScore = () => {
		let multi = 0;
		for (let i = 0; i < 199; i += indent) {
			const row = [
				i,
				i + 1,
				i + 2,
				i + 3,
				i + 4,
				i + 5,
				i + 6,
				i + 7,
				i + 8,
				i + 9,
			];
			if (row.every((ind) => squares[ind].classList.contains("taken"))) {
				var audio = new Audio("assets/fairydust.mp3");
				audio.play();
				audio.volume = 0.1;
				multi++;
				score += 10;
				lines += 1;
				linesDisplay.innerHTML = lines;
				row.forEach((index) => {
					squares[index].classList.remove("taken");
					squares[index].classList.remove("tetromino");
					squares[index].classList.remove(colors[random]);

					squares[index].classList = "";
				});
				const squaresRemoved = squares.splice(i, indent);
				squares = squaresRemoved.concat(squares);
				squares.forEach((cell) => grid.appendChild(cell));
			}
			// give extra points when doing multilines
			if (multi === 3) score += 10;
			if (multi === 4) score += 20;
			if (multi > 4) score += 40;
			// calculate total score by 100's
			const newlevel = Math.round(score / 100);
			// play sound
			if (newlevel > level) {
				var audio = new Audio("assets/4.mp3");
				audio.play();
				audio.volume = 0.1;
				level = newlevel;
			}
			// increase speed by 0.1 seconds per level
			speed = 1000 + 200 * level;

			scoreDisplay.innerHTML = score;
			levelDisplay.innerHTML = level;
		}
	};

	const gameOver = () => {
		if (
			currentTetromino.some((i) =>
				squares[currentPosition + i].classList.contains("taken"),
			)
		) {
			clearInterval(timerId);
		}
	};

	timerId = setInterval(moveDown, speed);
	miniDisplay();
});
